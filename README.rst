===============
NEMO Allauth
===============

NEMO plugin to add social account authentication support to your NEMO instance.

This plugin is a wrapper around the great django-allauth plugin, allowing you to setup any social login with NEMO effortlessly.

For more information on django-allauth please visit
https://django-allauth.readthedocs.io/

Description
============
This plugin"s goal is to allow users to sign in to NEMO with their social accounts.

Due to NEMO"s usage in labs to control area access and tool access, it is not advised to allow users to automatically signup.
For that reason, allauth signup is disabled.

The flow and process of adding users and having them log in via social credentials is a bit different:

1. New users can **only** be added from within NEMO itself.

2. The user"s email will be used to login and needs to be verified both in NEMO and by the social account provider (most do).

3. The user"s email address in NEMO needs to match the one provided by the social account.

Installation
============
1. install library

 ``pip install NEMO-allauth["socialaccount"]``

2. in settings.py add to your INSTALLED_APPS:

 ``"django.contrib.sites",``

 ...

 ``"NEMO",``

 ``"NEMO_allauth",`` (**has to be between NEMO and allauth**)

 ``"allauth",``

 ``"allauth.account",``

 ``"allauth.socialaccount",``

followed by the social account providers you'd like to use, for example:

 ``"allauth.socialaccount.providers.google",``

(A complete list with more detailed information is available at https://django-allauth.readthedocs.io/)

3. In settings.py add to your MIDDLEWARE:

 ``"allauth.account.middleware.AccountMiddleware"``

4. for each provider, you can set its configuration in settings.py:

::

    SOCIALACCOUNT_PROVIDERS = {
        "openid_connect": {
            "APPS": [
                {
                    "provider_id": "keycloak",
                    "name": "Keycloak",
                    "client_id": "<insert-id>",
                    "secret": "<insert-secret>",
                    "settings": {
                        "server_url": "<insert-url>/realms/<insert-realm>/.well-known/openid-configuration",
                    },
                }
            ]
        }
        "google": {
            "APP": {
                "client_id": "<client_id>",
                "secret": "<secret>"
            }
        }
    }

5. in settings.py set authentication backend to:

 ``AUTHENTICATION_BACKENDS = ["allauth.account.auth_backends.AuthenticationBackend"]``

6. in settings.py, set django's login and logout URLs to:

 ``LOGIN_URL = "account_login"``

 ``LOGIN_REDIRECT_URL = "landing"``

 ``LOGOUT_REDIRECT_URL = "account_logout"``

if you only support one social provider (google for example), you can bypass the login screen by setting:

 ``LOGIN_URL = "google_login"``

7. add to context_processors:

 ``"NEMO.context_processors.show_logout_button",``


Post Installation
=================

1. run:

    ``python manage.py migrate``

2. login to NEMO detailed administration and update the ``example.com`` site with the appropriate domain and display name (don't add the http part of your domain).


Customizing emails
==================
You can customize the emails being sent (a list can be found `here <https://django-allauth.readthedocs.io/en/latest/advanced.html#sending-email>`_)

To do that, place the filename in your NEMO `media` folder, for example to customize the subject `account/email/email_confirmation_subject.txt`, place `email_confirmation_subject.txt` in your `media` folder.

For emails, to customize `account/email/email_confirmation_message.txt`, place `email_confirmation_message.txt` to your `media` folder.

Both `.html` and `.txt` can be used for email messages.


Options
=======
By default, this plugin will send email verification emails to newly created users or users whose email has been changed.
You can turn this off by setting:

    ``SEND_EMAIL_VERIFICATION_ON_USER_SAVE = False``

By default, superuser email are considered verified. You can change that by adding in your settings.py:

    ``TRUST_SUPERUSERS_EMAIL = False``

You also can add a extra ajax url to be called on logout (before logout redirect url) by using:

    ``AJAX_BEFORE_LOGOUT_URL = <URL>``

Although not recommended, unverified emails coming from a specific provider (who for some reason cannot guarantee the email is owned by the user) can be marked as verified by setting the following:

::

    SOCIALACCOUNT_PROVIDERS = {
        "microsoft": {
            "trust_emails": True,
            ...
        }
    }



NEMO User admin
===============
In the detailed administration section of NEMO, actions were added to the User table. After selecting one or more users:

1. To send a verification email to the user's email address

2. To mark the user's email address as verified (use at your own risk)
